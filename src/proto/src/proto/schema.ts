/* eslint-disable */
import * as Long from "long";
import * as _m0 from "protobufjs/minimal";
import { Timestamp } from "../../google/protobuf/timestamp";

export const protobufPackage = "schema";

export interface EmptyProto {
}

export interface ClaimCreateProto {
  deliveryDate: Timestamp | undefined;
  deadlineDate: Timestamp | undefined;
}

export interface PriceOptionProto {
  price: number;
  itemsId: string;
}

export interface ItemProto {
  id: string;
}

export interface ItemConfigProto {
  itemId: string;
}

export interface ItemConfigFullProto {
  item: ItemProto | undefined;
}

export interface ClaimProto {
  id: string;
  itemsConfig: ItemConfigProto[];
  itemsConfigFull: ItemConfigFullProto[];
  priceOption: PriceOptionProto[];
}

export interface UserVerify {
  email: string;
}

export interface UserRegisterProto {
  email: string;
  password: string;
  code: string;
}

export interface UserAuthResponceProto {
  email: string;
  token: string;
}

export interface UserProto {
  email: string;
}

function createBaseEmptyProto(): EmptyProto {
  return {};
}

export const EmptyProto = {
  encode(_: EmptyProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): EmptyProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseEmptyProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(_: any): EmptyProto {
    return {};
  },

  toJSON(_: EmptyProto): unknown {
    const obj: any = {};
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<EmptyProto>, I>>(_: I): EmptyProto {
    const message = createBaseEmptyProto();
    return message;
  },
};

function createBaseClaimCreateProto(): ClaimCreateProto {
  return { deliveryDate: undefined, deadlineDate: undefined };
}

export const ClaimCreateProto = {
  encode(message: ClaimCreateProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.deliveryDate !== undefined) {
      Timestamp.encode(message.deliveryDate, writer.uint32(10).fork()).ldelim();
    }
    if (message.deadlineDate !== undefined) {
      Timestamp.encode(message.deadlineDate, writer.uint32(18).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ClaimCreateProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseClaimCreateProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.deliveryDate = Timestamp.decode(reader, reader.uint32());
          break;
        case 2:
          message.deadlineDate = Timestamp.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ClaimCreateProto {
    return {
      deliveryDate: isSet(object.deliveryDate) ? fromJsonTimestamp(object.deliveryDate) : undefined,
      deadlineDate: isSet(object.deadlineDate) ? fromJsonTimestamp(object.deadlineDate) : undefined,
    };
  },

  toJSON(message: ClaimCreateProto): unknown {
    const obj: any = {};
    message.deliveryDate !== undefined && (obj.deliveryDate = fromTimestamp(message.deliveryDate).toISOString());
    message.deadlineDate !== undefined && (obj.deadlineDate = fromTimestamp(message.deadlineDate).toISOString());
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ClaimCreateProto>, I>>(object: I): ClaimCreateProto {
    const message = createBaseClaimCreateProto();
    message.deliveryDate = (object.deliveryDate !== undefined && object.deliveryDate !== null)
      ? Timestamp.fromPartial(object.deliveryDate)
      : undefined;
    message.deadlineDate = (object.deadlineDate !== undefined && object.deadlineDate !== null)
      ? Timestamp.fromPartial(object.deadlineDate)
      : undefined;
    return message;
  },
};

function createBasePriceOptionProto(): PriceOptionProto {
  return { price: 0, itemsId: "" };
}

export const PriceOptionProto = {
  encode(message: PriceOptionProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.price !== 0) {
      writer.uint32(9).double(message.price);
    }
    if (message.itemsId !== "") {
      writer.uint32(18).string(message.itemsId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): PriceOptionProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBasePriceOptionProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.price = reader.double();
          break;
        case 2:
          message.itemsId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): PriceOptionProto {
    return {
      price: isSet(object.price) ? Number(object.price) : 0,
      itemsId: isSet(object.itemsId) ? String(object.itemsId) : "",
    };
  },

  toJSON(message: PriceOptionProto): unknown {
    const obj: any = {};
    message.price !== undefined && (obj.price = message.price);
    message.itemsId !== undefined && (obj.itemsId = message.itemsId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<PriceOptionProto>, I>>(object: I): PriceOptionProto {
    const message = createBasePriceOptionProto();
    message.price = object.price ?? 0;
    message.itemsId = object.itemsId ?? "";
    return message;
  },
};

function createBaseItemProto(): ItemProto {
  return { id: "" };
}

export const ItemProto = {
  encode(message: ItemProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ItemProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseItemProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ItemProto {
    return { id: isSet(object.id) ? String(object.id) : "" };
  },

  toJSON(message: ItemProto): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ItemProto>, I>>(object: I): ItemProto {
    const message = createBaseItemProto();
    message.id = object.id ?? "";
    return message;
  },
};

function createBaseItemConfigProto(): ItemConfigProto {
  return { itemId: "" };
}

export const ItemConfigProto = {
  encode(message: ItemConfigProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.itemId !== "") {
      writer.uint32(10).string(message.itemId);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ItemConfigProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseItemConfigProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.itemId = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ItemConfigProto {
    return { itemId: isSet(object.itemId) ? String(object.itemId) : "" };
  },

  toJSON(message: ItemConfigProto): unknown {
    const obj: any = {};
    message.itemId !== undefined && (obj.itemId = message.itemId);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ItemConfigProto>, I>>(object: I): ItemConfigProto {
    const message = createBaseItemConfigProto();
    message.itemId = object.itemId ?? "";
    return message;
  },
};

function createBaseItemConfigFullProto(): ItemConfigFullProto {
  return { item: undefined };
}

export const ItemConfigFullProto = {
  encode(message: ItemConfigFullProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.item !== undefined) {
      ItemProto.encode(message.item, writer.uint32(10).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ItemConfigFullProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseItemConfigFullProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.item = ItemProto.decode(reader, reader.uint32());
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ItemConfigFullProto {
    return { item: isSet(object.item) ? ItemProto.fromJSON(object.item) : undefined };
  },

  toJSON(message: ItemConfigFullProto): unknown {
    const obj: any = {};
    message.item !== undefined && (obj.item = message.item ? ItemProto.toJSON(message.item) : undefined);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ItemConfigFullProto>, I>>(object: I): ItemConfigFullProto {
    const message = createBaseItemConfigFullProto();
    message.item = (object.item !== undefined && object.item !== null) ? ItemProto.fromPartial(object.item) : undefined;
    return message;
  },
};

function createBaseClaimProto(): ClaimProto {
  return { id: "", itemsConfig: [], itemsConfigFull: [], priceOption: [] };
}

export const ClaimProto = {
  encode(message: ClaimProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.id !== "") {
      writer.uint32(10).string(message.id);
    }
    for (const v of message.itemsConfig) {
      ItemConfigProto.encode(v!, writer.uint32(18).fork()).ldelim();
    }
    for (const v of message.itemsConfigFull) {
      ItemConfigFullProto.encode(v!, writer.uint32(26).fork()).ldelim();
    }
    for (const v of message.priceOption) {
      PriceOptionProto.encode(v!, writer.uint32(34).fork()).ldelim();
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): ClaimProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseClaimProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.id = reader.string();
          break;
        case 2:
          message.itemsConfig.push(ItemConfigProto.decode(reader, reader.uint32()));
          break;
        case 3:
          message.itemsConfigFull.push(ItemConfigFullProto.decode(reader, reader.uint32()));
          break;
        case 4:
          message.priceOption.push(PriceOptionProto.decode(reader, reader.uint32()));
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): ClaimProto {
    return {
      id: isSet(object.id) ? String(object.id) : "",
      itemsConfig: Array.isArray(object?.itemsConfig)
        ? object.itemsConfig.map((e: any) => ItemConfigProto.fromJSON(e))
        : [],
      itemsConfigFull: Array.isArray(object?.itemsConfigFull)
        ? object.itemsConfigFull.map((e: any) => ItemConfigFullProto.fromJSON(e))
        : [],
      priceOption: Array.isArray(object?.priceOption)
        ? object.priceOption.map((e: any) => PriceOptionProto.fromJSON(e))
        : [],
    };
  },

  toJSON(message: ClaimProto): unknown {
    const obj: any = {};
    message.id !== undefined && (obj.id = message.id);
    if (message.itemsConfig) {
      obj.itemsConfig = message.itemsConfig.map((e) => e ? ItemConfigProto.toJSON(e) : undefined);
    } else {
      obj.itemsConfig = [];
    }
    if (message.itemsConfigFull) {
      obj.itemsConfigFull = message.itemsConfigFull.map((e) => e ? ItemConfigFullProto.toJSON(e) : undefined);
    } else {
      obj.itemsConfigFull = [];
    }
    if (message.priceOption) {
      obj.priceOption = message.priceOption.map((e) => e ? PriceOptionProto.toJSON(e) : undefined);
    } else {
      obj.priceOption = [];
    }
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<ClaimProto>, I>>(object: I): ClaimProto {
    const message = createBaseClaimProto();
    message.id = object.id ?? "";
    message.itemsConfig = object.itemsConfig?.map((e) => ItemConfigProto.fromPartial(e)) || [];
    message.itemsConfigFull = object.itemsConfigFull?.map((e) => ItemConfigFullProto.fromPartial(e)) || [];
    message.priceOption = object.priceOption?.map((e) => PriceOptionProto.fromPartial(e)) || [];
    return message;
  },
};

function createBaseUserVerify(): UserVerify {
  return { email: "" };
}

export const UserVerify = {
  encode(message: UserVerify, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.email !== "") {
      writer.uint32(10).string(message.email);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserVerify {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUserVerify();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.email = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserVerify {
    return { email: isSet(object.email) ? String(object.email) : "" };
  },

  toJSON(message: UserVerify): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UserVerify>, I>>(object: I): UserVerify {
    const message = createBaseUserVerify();
    message.email = object.email ?? "";
    return message;
  },
};

function createBaseUserRegisterProto(): UserRegisterProto {
  return { email: "", password: "", code: "" };
}

export const UserRegisterProto = {
  encode(message: UserRegisterProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.email !== "") {
      writer.uint32(10).string(message.email);
    }
    if (message.password !== "") {
      writer.uint32(18).string(message.password);
    }
    if (message.code !== "") {
      writer.uint32(26).string(message.code);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserRegisterProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUserRegisterProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.email = reader.string();
          break;
        case 2:
          message.password = reader.string();
          break;
        case 3:
          message.code = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserRegisterProto {
    return {
      email: isSet(object.email) ? String(object.email) : "",
      password: isSet(object.password) ? String(object.password) : "",
      code: isSet(object.code) ? String(object.code) : "",
    };
  },

  toJSON(message: UserRegisterProto): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    message.password !== undefined && (obj.password = message.password);
    message.code !== undefined && (obj.code = message.code);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UserRegisterProto>, I>>(object: I): UserRegisterProto {
    const message = createBaseUserRegisterProto();
    message.email = object.email ?? "";
    message.password = object.password ?? "";
    message.code = object.code ?? "";
    return message;
  },
};

function createBaseUserAuthResponceProto(): UserAuthResponceProto {
  return { email: "", token: "" };
}

export const UserAuthResponceProto = {
  encode(message: UserAuthResponceProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.email !== "") {
      writer.uint32(10).string(message.email);
    }
    if (message.token !== "") {
      writer.uint32(18).string(message.token);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserAuthResponceProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUserAuthResponceProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.email = reader.string();
          break;
        case 2:
          message.token = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserAuthResponceProto {
    return {
      email: isSet(object.email) ? String(object.email) : "",
      token: isSet(object.token) ? String(object.token) : "",
    };
  },

  toJSON(message: UserAuthResponceProto): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    message.token !== undefined && (obj.token = message.token);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UserAuthResponceProto>, I>>(object: I): UserAuthResponceProto {
    const message = createBaseUserAuthResponceProto();
    message.email = object.email ?? "";
    message.token = object.token ?? "";
    return message;
  },
};

function createBaseUserProto(): UserProto {
  return { email: "" };
}

export const UserProto = {
  encode(message: UserProto, writer: _m0.Writer = _m0.Writer.create()): _m0.Writer {
    if (message.email !== "") {
      writer.uint32(10).string(message.email);
    }
    return writer;
  },

  decode(input: _m0.Reader | Uint8Array, length?: number): UserProto {
    const reader = input instanceof _m0.Reader ? input : new _m0.Reader(input);
    let end = length === undefined ? reader.len : reader.pos + length;
    const message = createBaseUserProto();
    while (reader.pos < end) {
      const tag = reader.uint32();
      switch (tag >>> 3) {
        case 1:
          message.email = reader.string();
          break;
        default:
          reader.skipType(tag & 7);
          break;
      }
    }
    return message;
  },

  fromJSON(object: any): UserProto {
    return { email: isSet(object.email) ? String(object.email) : "" };
  },

  toJSON(message: UserProto): unknown {
    const obj: any = {};
    message.email !== undefined && (obj.email = message.email);
    return obj;
  },

  fromPartial<I extends Exact<DeepPartial<UserProto>, I>>(object: I): UserProto {
    const message = createBaseUserProto();
    message.email = object.email ?? "";
    return message;
  },
};

export interface ClaimServiceProto {
  createOne(request: ClaimCreateProto): Promise<ClaimProto>;
}

export class ClaimServiceProtoClientImpl implements ClaimServiceProto {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.createOne = this.createOne.bind(this);
  }
  createOne(request: ClaimCreateProto): Promise<ClaimProto> {
    const data = ClaimCreateProto.encode(request).finish();
    const promise = this.rpc.request("schema.ClaimServiceProto", "createOne", data);
    return promise.then((data) => ClaimProto.decode(new _m0.Reader(data)));
  }
}

export interface UserServiceProto {
  register(request: UserRegisterProto): Promise<UserAuthResponceProto>;
  verifyEmail(request: UserVerify): Promise<EmptyProto>;
}

export class UserServiceProtoClientImpl implements UserServiceProto {
  private readonly rpc: Rpc;
  constructor(rpc: Rpc) {
    this.rpc = rpc;
    this.register = this.register.bind(this);
    this.verifyEmail = this.verifyEmail.bind(this);
  }
  register(request: UserRegisterProto): Promise<UserAuthResponceProto> {
    const data = UserRegisterProto.encode(request).finish();
    const promise = this.rpc.request("schema.UserServiceProto", "register", data);
    return promise.then((data) => UserAuthResponceProto.decode(new _m0.Reader(data)));
  }

  verifyEmail(request: UserVerify): Promise<EmptyProto> {
    const data = UserVerify.encode(request).finish();
    const promise = this.rpc.request("schema.UserServiceProto", "verifyEmail", data);
    return promise.then((data) => EmptyProto.decode(new _m0.Reader(data)));
  }
}

interface Rpc {
  request(service: string, method: string, data: Uint8Array): Promise<Uint8Array>;
}

type Builtin = Date | Function | Uint8Array | string | number | boolean | undefined;

export type DeepPartial<T> = T extends Builtin ? T
  : T extends Long ? string | number | Long : T extends Array<infer U> ? Array<DeepPartial<U>>
  : T extends ReadonlyArray<infer U> ? ReadonlyArray<DeepPartial<U>>
  : T extends {} ? { [K in keyof T]?: DeepPartial<T[K]> }
  : Partial<T>;

type KeysOfUnion<T> = T extends T ? keyof T : never;
export type Exact<P, I extends P> = P extends Builtin ? P
  : P & { [K in keyof P]: Exact<P[K], I[K]> } & { [K in Exclude<keyof I, KeysOfUnion<P>>]: never };

function toTimestamp(date: Date): Timestamp {
  const seconds = numberToLong(date.getTime() / 1_000);
  const nanos = (date.getTime() % 1_000) * 1_000_000;
  return { seconds, nanos };
}

function fromTimestamp(t: Timestamp): Date {
  let millis = t.seconds.toNumber() * 1_000;
  millis += t.nanos / 1_000_000;
  return new Date(millis);
}

function fromJsonTimestamp(o: any): Timestamp {
  if (o instanceof Date) {
    return toTimestamp(o);
  } else if (typeof o === "string") {
    return toTimestamp(new Date(o));
  } else {
    return Timestamp.fromJSON(o);
  }
}

function numberToLong(number: number) {
  return Long.fromNumber(number);
}

// If you get a compile-error about 'Constructor<Long> and ... have no overlap',
// add '--ts_proto_opt=esModuleInterop=true' as a flag when calling 'protoc'.
if (_m0.util.Long !== Long) {
  _m0.util.Long = Long as any;
  _m0.configure();
}

function isSet(value: any): boolean {
  return value !== null && value !== undefined;
}
