import { WithId, Document, Collection, AggregateOptions } from 'mongodb';

export class BaseMongoService<T extends Document> {
  public collection: Collection<T>;

  async findOne(obj: Partial<T>): Promise<WithId<T>> {
    return this.collection.findOne(obj);
  }

  async findOneWithLookups(
    pipeline?: Document[],
    options?: AggregateOptions,
  ): Promise<T>;

  async findOneWithLookups<R>(
    pipeline?: Document[],
    options?: AggregateOptions,
  ): Promise<R | T> {
    const _cursor = this.collection.aggregate<R | T>(pipeline, options);
    const val = await _cursor.next();
    _cursor.close();
    return val;
  }
}
