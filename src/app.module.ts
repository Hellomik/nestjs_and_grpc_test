import { CacheModule, Module } from '@nestjs/common';
import { ClaimModule } from './modules/claim/claim.module';
import { HelloWorldController } from './modules/hello_world/hello_world.controller';
import { MongoModule } from './modules/mongo/mongo.module';
import { ProtoModule } from './modules/proto/proto.module';
import {
  GrpcReflectionModule,
  addReflectionToGrpcConfig,
} from 'nestjs-grpc-reflection';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';
import { ItemModule } from './modules/item/item.module';
import { JwtModule } from '@nestjs/jwt';
import * as fs from 'fs';
import { UserModule } from './modules/user/user.module';

export const grpcOptions = addReflectionToGrpcConfig({
  transport: Transport.GRPC,
  options: {
    url: 'localhost:8001',
    package: 'schema',
    protoPath: join(__dirname, 'proto/schema.proto'),
  },
});
@Module({
  imports: [
    CacheModule.register({
      isGlobal: true,
      ttl: 10000,
    }),
    GrpcReflectionModule.register(grpcOptions),
    {
      ...JwtModule.register({
        privateKey: fs.readFileSync(join(__dirname, 'jwt/jwtRS256.key')),
        publicKey: fs.readFileSync(join(__dirname, 'jwt/jwtRS256.key.pub')),
        signOptions: { expiresIn: '340d', algorithm: 'RS256' },
      }),
      global: true,
    },
    UserModule,
    MongoModule,
    ClaimModule,
    ProtoModule,
    ItemModule,
  ],
  controllers: [HelloWorldController],
  providers: [HelloWorldController],
})
export class AppModule {}
