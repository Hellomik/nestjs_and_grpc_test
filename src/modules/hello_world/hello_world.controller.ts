import { Controller, Get } from '@nestjs/common';

@Controller()
export class HelloWorldController {
  constructor() {}

  @Get()
  helloWorld(): string {
    return 'Hello World!';
  }
}
