import { MongooseModule } from '@nestjs/mongoose';

export const MongoModule = MongooseModule.forRoot(
  'mongodb://hellomik:a3fb56f5-544d-4a9f-a638-49e0e405abb6@localhost:27017/?readPreference=primary&directConnection=true',
  {
    dbName: 'foru',
  },
);
