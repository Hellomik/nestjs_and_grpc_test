import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { ObjectId } from 'mongodb';
import { ItemProto } from 'src/proto/src/proto/schema';
export type ItemDocument = Item & mongoose.Document;

export interface ItemInterface {
  _id: ObjectId;
}

@Schema()
export class Item implements ItemInterface {
  @Prop()
  _id: ObjectId;

  constructor(obj: ItemInterface) {
    this._id = obj._id;
  }
}

export class ItemMessage implements ItemProto {
  id: string;
  constructor(obj: ItemInterface) {
    this.id = obj._id.toString();
  }
}

export const ItemSchema = SchemaFactory.createForClass(Item);
