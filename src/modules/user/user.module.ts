import { CacheModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserGrpcController } from './grpc_controller/userGrpc.controller';
import { User, UserSchema } from './model/user.model';
import { AuthenticationService } from './service/authentication.service';
import { RegisterService } from './service/register.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  controllers: [UserGrpcController],
  providers: [AuthenticationService, RegisterService],
})
export class UserModule {}
