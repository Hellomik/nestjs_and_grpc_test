import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ObjectId } from 'mongodb';
import mongoose from 'mongoose';
import { UserProto } from 'src/proto/src/proto/schema';

export type UserDocument = User & mongoose.Document;

export interface UserInterface {
  _id: ObjectId;
  email: string;
  passwordHash: string;
}

@Schema()
export class User implements UserInterface {
  @Prop()
  _id: ObjectId;

  @Prop()
  email: string;

  @Prop()
  passwordHash: string;

  constructor(obj: UserInterface) {
    this._id = obj._id;
    this.email = obj.email;
    this.passwordHash = obj.passwordHash;
  }

  toUserProto(): UserProto {
    return UserProto.fromPartial({
      email: this.email,
    });
  }
}

export const UserSchema = SchemaFactory.createForClass(User);
