import { CACHE_MANAGER, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Cache } from 'cache-manager';

@Injectable()
export class AuthenticationService {
  constructor(
    private jwtService: JwtService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
  ) {}

  async sign(obj: { id: string; [key: string]: any }) {
    return this.jwtService.sign(obj);
  }

  async verify(token: string) {
    // this.jwtSeуrvice.
    return this.jwtService.verify(token);
  }

  async sendVerificationCode(obj: { email: string }) {
    const { email } = obj;
    await this.cacheManager.set(email, `${1111}`, 10000);
    console.log(await this.cacheManager.get<string>(email));
  }

  async verifyVerificationCode(obj: {
    code: string;
    email: string;
  }): Promise<boolean> {
    const value = await this.cacheManager.get<string>(obj.email);
    return value == obj.code; 
  }
}
