import { Injectable } from '@nestjs/common';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Collection, ObjectId } from 'mongodb';
import { BaseMongoService } from 'src/core/base_mongo_service';
import { User, UserDocument } from '../model/user.model';
import { AuthenticationService } from './authentication.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class RegisterService extends BaseMongoService<User> {
  constructor(
    @InjectModel(User.name) private userModel: mongoose.Model<UserDocument>,
    @InjectConnection() private connection: mongoose.Connection,
    private authenticationService: AuthenticationService,
  ) {
    super();
    this.collection = this.connection.db.collection<User>(
      'users',
    ) as Collection<User>;
  }

  async registerUser(__: { email: string; password: string }): Promise<User> {
    const { email, password } = __;
    const userCheck = await this.collection.findOne({
      email,
    });
    if (userCheck != null) {
      throw 'User Exist';
    }
    const passwordHash = await bcrypt.hash(password, await bcrypt.genSalt());
    const user = new User({
      email,
      passwordHash,
      _id: new ObjectId(),
    });
    await this.collection.insertOne(user);
    return user;
  }
}
