import { Metadata, ServerUnaryCall } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import {
  EmptyProto,
  UserAuthResponceProto,
  UserRegisterProto,
  UserServiceProto,
  UserVerify,
} from 'src/proto/src/proto/schema';
import { AuthenticationService } from '../service/authentication.service';
import { RegisterService } from '../service/register.service';

const serviceName = 'UserServiceProto';

@Controller()
export class UserGrpcController implements UserServiceProto {
  constructor(
    private registerService: RegisterService,
    private authService: AuthenticationService,
  ) {}

  @GrpcMethod(serviceName, 'verifyEmail')
  async verifyEmail(request: UserVerify): Promise<EmptyProto> {
    await this.authService.sendVerificationCode({
      email: request.email,
    });
    return EmptyProto.fromPartial({});
  }

  @GrpcMethod(serviceName, 'register')
  async register(
    request: UserRegisterProto,
    metadata?: Metadata,
    call?: ServerUnaryCall<any, any>,
  ): Promise<UserAuthResponceProto> {
    const { email: _email, password, code } = request;
    const email = _email.replace(' ', '').trim();

    if (
      !(await this.authService.verifyVerificationCode({
        email,
        code,
      }))
    ) {
      throw 'ERROR check';
    }
    const user = await this.registerService.registerUser({
      email,
      password,
    });

    const token = await this.authService.sign({
      id: user._id.toHexString(),
      email,
    });
    return UserAuthResponceProto.fromPartial({
      email: user.email,
      token,
    });
  }
}
