import { Injectable } from '@nestjs/common';
import { BaseMongoService } from 'src/core/base_mongo_service';
import { Claim, ClaimDocument, ClaimInterface } from '../models/claim.model';
import { ObjectId, Collection } from 'mongodb';
import { InjectConnection, InjectModel } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Injectable()
export class MainClaimService extends BaseMongoService<Claim> {
  constructor(
    @InjectModel(Claim.name) private claimModel: mongoose.Model<ClaimDocument>,
    @InjectConnection() private connection: mongoose.Connection,
  ) {
    super();
    this.collection = this.connection.db.collection<Claim>(
      'claim',
    ) as Collection<Claim>;
  }

  async createClaim(__: { claim: ClaimInterface }): Promise<Claim> {
    return new this.claimModel(new Claim(__.claim)).save();
  }
}
