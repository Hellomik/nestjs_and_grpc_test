import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ClaimController } from './grpc_controller/claimGrpc.controller';
// import { ClaimProtoBridge } from './bridges/proto.bridge';
import { Claim, ClaimSchema } from './models/claim.model';
import { MainClaimService } from './services/main_claim.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Claim.name, schema: ClaimSchema }]),
  ],
  controllers: [ClaimController],
  providers: [MainClaimService],
  exports: [MainClaimService],
})
export class ClaimModule {}
