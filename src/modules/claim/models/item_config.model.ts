import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ObjectId } from 'mongodb';
import {
  Item,
  ItemInterface,
  ItemMessage,
  ItemSchema,
} from 'src/modules/item/models/item.model';
import { ItemConfigProto } from 'src/proto/src/proto/schema';

export interface ItemConfigInterface {
  itemId: ObjectId;
}

@Schema()
export class ItemConfig implements ItemConfigInterface {
  @Prop({ type: [ObjectId] })
  itemId: ObjectId;

  constructor(obj: ItemConfigInterface) {
    this.itemId = obj.itemId;
  }
}

export class ItemConfigMessage implements ItemConfigProto {
  itemId: string;
  constructor(obj: ItemConfigInterface) {
    this.itemId = obj.itemId.toHexString();
  }
}

export const ItemConfigSchema = SchemaFactory.createForClass(ItemConfig);
