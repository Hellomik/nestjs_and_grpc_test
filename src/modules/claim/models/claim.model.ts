import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { ObjectId } from 'mongodb';
import {
  ItemConfig,
  ItemConfigInterface,
  ItemConfigMessage,
} from './item_config.model';
import { PriceOption, PriceOptionInterface } from './price_option.model';
import { ClaimProto } from 'src/proto/src/proto/schema';

export type ClaimDocument = Claim & mongoose.Document;

export interface ClaimInterface {
  _id: ObjectId;
  items: ItemConfigInterface[];
  delivaryDate: Date;
  deadlineDate: Date;
  priceOptions: PriceOptionInterface[];
}

@Schema()
export class Claim implements ClaimInterface {
  @Prop()
  _id: ObjectId;

  @Prop()
  items: ItemConfig[];

  @Prop({ type: mongoose.Types.Map, required: true })
  delivaryDate: Date;

  @Prop()
  deadlineDate: Date;

  @Prop({
    type: PriceOption,
  })
  priceOptions: PriceOptionInterface[];

  constructor(obj: ClaimInterface) {
    this._id = obj._id;
    this.items = obj.items.map((e) => new ItemConfig(e));
    this.delivaryDate = obj.delivaryDate;
    this.deadlineDate = obj.deadlineDate;
  }

  toClaimProto(__?: {}): ClaimProto {
    return ClaimProto.fromPartial({
      id: this._id.toHexString(),
    });
  }
}

export const ClaimSchema = SchemaFactory.createForClass(Claim);
