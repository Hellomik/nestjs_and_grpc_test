import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

export interface PriceOptionInterface {
  price: number;
  itemsId: string;
}

@Schema()
export class PriceOption implements PriceOptionInterface {
  @Prop()
  price: number;

  @Prop()
  itemsId: string;
}

export const PriceOptionSchema = SchemaFactory.createForClass(PriceOption);
