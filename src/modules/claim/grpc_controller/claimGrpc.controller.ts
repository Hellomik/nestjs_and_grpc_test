import { Metadata, MetadataValue, ServerUnaryCall } from '@grpc/grpc-js';
import { Controller } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';
import { ObjectId } from 'mongodb';
import {
  ClaimCreateProto,
  ClaimProto,
  ClaimServiceProto,
  EmptyProto,
} from 'src/proto/src/proto/schema';
import { MainClaimService } from '../services/main_claim.service';

const serviceName = 'ClaimServiceProto';

@Controller()
export class ClaimController implements ClaimServiceProto {
  constructor(private mainClaimService: MainClaimService) {}

  @GrpcMethod(serviceName, 'createOne')
  async createOne(
    data: ClaimCreateProto,
    metadata?: Metadata,
    call?: ServerUnaryCall<any, any>,
  ): Promise<ClaimProto> {
    const newClaim = await this.mainClaimService.createClaim({
      claim: {
        _id: new ObjectId(),
        items: [],
        deadlineDate: new Date(data.deadlineDate.seconds.toNumber()),
        delivaryDate: new Date(data.deadlineDate.seconds.toNumber()),
        priceOptions: [],
      },
    });
    return newClaim.toClaimProto();
  }
}
