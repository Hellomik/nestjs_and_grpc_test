import { Injectable } from '@nestjs/common';
import { MainClaimService } from 'src/modules/claim/services/main_claim.service';

@Injectable()
export class ProtoService {
  constructor(private createClaimService: MainClaimService) {}
}
