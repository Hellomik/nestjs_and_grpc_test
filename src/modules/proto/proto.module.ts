import { Global, Module } from '@nestjs/common';
import { ClaimModule } from '../claim/claim.module';
// import { ProtoController } from './controller/proto.controller';
import { ProtoService } from './service/proto.service';

@Global()
@Module({
  imports: [ClaimModule],
  // controllers: [ProtoController],
  providers: [ProtoService],
})
export class ProtoModule {}
