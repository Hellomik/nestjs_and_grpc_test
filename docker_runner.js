const { exec } = require('child_process');

if (process.platform === 'win32' || process.platform === 'darwin') {
  exec('docker compose -f docker-compose.yml up -d');
} else {
  exec('docker compose -f docker-compose_ubuntu.yml up -d');
}
